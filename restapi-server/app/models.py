import requests
import json
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class ZeroTier(object):
    def __init__(self, netID, token):
        self.netID = netID
        self.token = token
        self.baseUrl = "https://my.zerotier.com/api/v1"
    def request(self, path):
        return requests.get(self.baseUrl+path, headers={'Authorization': 'Bearer '+ self.token})
    def getIP(self):
        data = self.request("/network/{0}/member".format(self.netID)).json()
        data = data[0]
        config = data["config"]
        return config["ipAssignments"]
    def status(self):
        data = self.request("/status").json()
        return data["online"]


class UserForm(FlaskForm):
    netID = StringField("Net ID", validators=[DataRequired()])
    token = StringField("Token", validators=[DataRequired()])
    getip = SubmitField("Get ip's in lan")
    status = SubmitField("Get Status")

#netID = "17d709436c4b3366"
#token = "vslkA5IhMxsSCXiJllip3IZGwbllXQwR"
#daniel = ZeroTier(netID, token)
#print(daniel.getIP())
#print((daniel.status()))

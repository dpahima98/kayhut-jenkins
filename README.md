# RestAPI

This project create a python:3.7 docker image with zerotier restAPI 

## Installation

Use Git to clone the project 

```bash
git clone https://gitlab.com/dpahima98/kayhut-jenkins.git
```

To use the docker run :

```bash
docker pull 10070/homework
docer run -d -it <IMAGE ID> /bin/bash
docker exec -it <CONTAINER ID> /bin/bash
```

##OR

Just run th sxript build.sh
```bash
./build
```